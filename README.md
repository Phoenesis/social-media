Bugs :
- Fix problem profile creation date (DB)
- Fix problem load an image in a post (see Blob, 
URL.createObjectURL + remove) so maybe back-end problem
- Fix problem message "Unknown password"
- Fix problem change our own profile picture (back-end problem)

To Do Next : 
- Add an "About me" page to add sources of the project 
- Add a "404" page
- Commit the project on GitHub
- Add CSS for confirmation alerts
- Add a drifted anchor to go up the posts page
- Offer a strong passwords manager
- Change our own password
- Change our own pseudo
- Verify the mobile version
- Make the number of suggestions responsive (to not 
refresh the page each time)
- Delete our account (so our posts, comments...)
- Pin a post
- Create surveys
- Report someone / a comment / a post
- Tag a person
- Make a place where handle our posts / comments -> page settings
- Make a dark mode (activate it in profile -> settings)
- Choose language easily (French / English)
- Define the cookie jwt (sameSite Strict...)
- Block a person
- React with emojis
- Do a "admin" / "moderator" role to delete / warn a person
- Make a bot on the right bottom to help new people 
(some questions / answers)


###################################################

Bugs fixed : 

"To Do" made : 
