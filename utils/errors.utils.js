module.exports.signUpErrors = (err) => {
    let errors = {pseudo: "", email: "", password: ""}

    if (err.message.includes("pseudo")) {
        errors.pseudo = "Wrong pseudo or already taken"
    }
    if (err.message.includes("email")) {
        errors.email = "Wrong email"
    }
    if (err.message.includes("password")) {
        errors.password = "Wrong password : minimum 6 characters"
    }
    if (err.code === 11000 && Object.keys(err.keyValue)[0].includes("pseudo")) {
        errors.pseudo = "This pseudo is already taken"
    }
    if (err.code === 11000 && Object.keys(err.keyValue)[0].includes("email")) {
        errors.email = "This email is already taken"
    }
    return errors
}

module.exports.signInErrors = (err) => {
    let errors = {email: "", password: ""}

    if (err.message.includes("email")) {
        errors.email = "Unknown email"
    }

    if (err.message.includes("password")) {
        errors.password = "The password doesn't correspond"
    }
    return errors
}

module.exports.uploadErrors = (err) => {
    let errors = {format: "", maxSize: ""}
    if (err.message.includes('invalid file')) {
        errors.format = "Incompatible file"
    }
    if (err.message.includes('max size')) {
        errors.maxSize = "The size of the file is over 5 kB"
    }
    return errors
}
