const express = require('express')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const userRoutes = require('./routes/user.routes')
const postRoutes = require('./routes/post.routes')
const {checkUser, requireAuth} = require('./middlewares/auth.middleware')
require('dotenv').config({path: './config/.env'})
require('./config/database')
const app = express()
const port = process.env.PORT

const corsOptions = {
    origin: [process.env.CLIENT_URL, process.env.LOCAL_URL],
    credentials: true,
    'allowedHeaders': ['sessionId', 'Content-Type'],
    'exposedHeaders': ['sessionId'],
    'methods': 'GET, HEAD, PUT, PATCH, POST, DELETE',
    'preflightContinue': false
}
app.use(cors(corsOptions))

app.use(express.json());
app.use(express.urlencoded({ extended: true}));
app.use(cookieParser())

//jwt
app.get('*', checkUser)
app.get('/jwtid', requireAuth)

// routes
app.use('/api/user', userRoutes)
app.use('/api/post', postRoutes)

// server
app.listen(port, () => console.log(`Listening on port ${port}`))