const UserModel = require('../models/user.model')
const jwt = require('jsonwebtoken')
const {signUpErrors, signInErrors} = require("../utils/errors.utils");

const MAX_AGE = 3 * 3600 * 1000 // 3 hours

const tokenFunction = (id) => {
    return new Promise((resolve, reject) => {
        jwt.sign({id}, process.env.TOKEN_SECRET,{expiresIn: MAX_AGE}, (err, token) => {
            if (err) {
                reject(err)
            } else {
                resolve(token)
            }
        })
    });
}

module.exports.signIn = async (req, res) => {
    const {email, password} = req.body
    try {
        const user = await UserModel.login(email, password)
        const token = await tokenFunction(user._id)
        res.cookie('jwt', token, {httpOnly: true, maxAge: MAX_AGE})
        res.status(200).json({user: user._id})
    } catch (err) {
        const errors = signInErrors(err)
        res.status(200).send({errors})
    }
}

module.exports.signUp = async (req, res) => {
    const {pseudo, email, password} = req.body
    try {
        const user = await UserModel.create({pseudo, email, password});
        res.status(201).json({user: user._id});
    } catch (err) {
        const errors = signUpErrors(err)
        res.status(200).send({errors})
    }
}

module.exports.logOut = (req, res) => {
    res.cookie('jwt', '', {maxAge: 1})
    res.redirect("/")
}