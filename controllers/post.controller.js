const ObjectID = require('mongoose').Types.ObjectId
const PostModel = require('../models/post.model')
const UserModel = require('../models/user.model')
const {uploadErrors} = require("../utils/errors.utils");
const fs = require("fs");

module.exports.readPost = (req, res) => {
    PostModel.find((err, docs) => {
        if (!err) {
            res.send(docs)
        } else {
            console.log("Error to get data : " + err)
        }
    }).sort({createdAt: -1})
}

module.exports.createPost = async (req, res) => {
    const fileName = req.body.posterId + Date.now() + ".jpg"
    const filePath = `${__dirname}\\..\\client\\public\\uploads\\posts\\${fileName}`
    const filePathDatabase = `.\\uploads\\posts\\${fileName}`
    if (req.file !== null && req.file !== undefined) {
        try {
            if (req.file.mimetype !== "image/png" && req.file.mimetype !== "image/jpg" && req.file.mimetype !== "image/jpeg") {
                throw Error("invalid file")
            }
            if (req.file.size > 5000000) {
                throw Error("max size")
            }
        } catch (err) {
            const errors = uploadErrors(err)
            return res.status(201).json(errors)
        }
        fs.writeFileSync(filePath, req.file.buffer)
    }
    try {
        const newPost = {
            posterId: req.body.posterId,
            message: req.body.message ? req.body.message : "",
            video: req.body.video ? req.body.video : "",
            picture: req.file ? filePathDatabase : "",
            likers: [],
            comments: [],
        }
        const post = await PostModel.create(newPost)
        return res.status(201).json(post)
    } catch (err) {
        return res.status(400).send(err)
    }
}

module.exports.updatePost = (req, res) => {
    if (!ObjectID.isValid(req.params.id)) {
        return res.status(400).send("Unknown ID : " + req.params.id)
    }
    const updatedRecord = {
        message: req.body.message
    }
    PostModel.findByIdAndUpdate(
        req.params.id,
        {$set: updatedRecord},
        {new: true},
        (err, docs) => {
            if (!err) {
                res.send(docs)
            } else {
                console.log("Update error : " + err)
            }
        }
    ).clone()
}

module.exports.deletePost = async (req, res) => {
    if (!ObjectID.isValid(req.params.id)) {
        return res.status(400).send("Unknown ID : " + req.params.id)
    }
    PostModel.findById({_id: req.params.id}, (err, docs) => {
        if (err) {
            console.error(err)
        } else {
            try {
                fs.unlinkSync(__dirname + "\\..\\client\\public\\" + docs.picture)

            } catch (err) {
                console.error(err)
            }

        }
    })
    PostModel.findByIdAndRemove(
        {_id: req.params.id},
        {},
        (err, docs) => {
            if (!err) {
                res.send(docs)
            } else {
                console.log("Delete error : " + err)
            }
        }
    )
}

module.exports.likePost = async (req, res) => {
    if (!ObjectID.isValid(req.params.id)) {
        return res.status(400).send("Unknown ID : " + req.params.id)
    }
    try {
        await PostModel.findByIdAndUpdate(
            {_id: req.params.id},
            {$addToSet: {likers: req.body.id}},
            {new: true},
            (err) => {
                if (err) {
                    return res.status(400).send(err)
                }
            }
        ).clone()
        await UserModel.findByIdAndUpdate(
            req.body.id,
            {$addToSet: {likers: req.params.id}},
            {new: true},
            (err, docs) => {
                if (!err) {
                    res.send(docs)
                } else {
                    return res.status(400).send(err)
                }
            }
        ).clone()
    } catch (err) {
        console.log(err)
    }
}

module.exports.unlikePost = async (req, res) => {
    if (!ObjectID.isValid(req.params.id)) {
        return res.status(400).send("Unknown ID : " + req.params.id)
    }
    try {
        await PostModel.findByIdAndUpdate(
            req.params.id,
            {$pull: {likers: req.body.id}},
            {new: true},
            (err) => {
                if (err) {
                    return res.status(400).send(err)
                }
            }
        ).clone()
        await UserModel.findByIdAndUpdate(
            {_id: req.body.id},
            {$pull: {likers: req.params.id}},
            {new: true},
            (err, docs) => {
                if (!err) {
                    res.send(docs)
                } else {
                    return res.status(400).send(err)
                }
            }
        ).clone()
    } catch (err) {
        console.log(err)
    }
}

module.exports.commentPost = (req, res) => {
    if (!ObjectID.isValid(req.params.id)) {
        return res.status(400).send("Unknown ID : " + req.params.id)
    }
    try {
        return PostModel.findByIdAndUpdate(
            {_id: req.params.id},
            {$push: {
                comments: {
                    commenterId: req.body.commenterId,
                    commenterPseudo: req.body.commenterPseudo,
                    text: req.body.text,
                    timestamp: new Date().getTime()
                }
            }},
            {new: true},
            (err, docs) => {
                if (!err) {
                    return res.send(docs)
                } else {
                    return res.status(400).send(err)
                }
            }
        ).clone()
    } catch (err) {
        return res.status(400).send(err)
    }
}

module.exports.editCommentPost = (req, res) => {
    if (!ObjectID.isValid(req.params.id)) {
        return res.status(400).send("Unknown ID : " + req.params.id)
    }
    try {
        return PostModel.findById(
            req.params.id,
            (err, docs) => {
                const theComment = docs.comments.find(comment => comment._id.equals(req.body.commentId))
                if (!theComment) {
                    return res.status(404).send('Comment not found')
                }
                theComment.text = req.body.text
                return docs.save(err => {
                    if (!err) {
                        return res.status(200).send(docs)
                    } else {
                        return res.status(500).send(err)
                    }
                })
            }
        ).clone()
    } catch (err) {
        return res.status(400).send(err)
    }
}

module.exports.deleteCommentPost = async (req, res) => {
    if (!ObjectID.isValid(req.params.id)) {
        return res.status(400).send("Unknown ID : " + req.params.id)
    }
    try {
        return PostModel.findByIdAndUpdate(
            req.params.id,
            {$pull: {comments: {_id: req.body.commentId}}},
            {new: true},
            (err, docs) => {
                if (!err) {
                    return res.send(docs)
                } else {
                    return res.status(400).send(err)
                }
            }
        ).clone()
    } catch (err) {
        return res.status(400).send(err)
    }
}
