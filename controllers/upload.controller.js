const fs = require("fs");
const UserModel = require("../models/user.model");
const {uploadErrors} = require("../utils/errors.utils");


module.exports.uploadProfile = async (req, res) => {
    try {
        if (req.file.mimetype !== "image/png" && req.file.mimetype !== "image/jpg" && req.file.mimetype !== "image/jpeg") {
            throw Error("invalid file")
        }
        if (req.file.size > 5000000) {
            throw Error("max size")
        }
    } catch (err) {
        const errors = uploadErrors(err)
        return res.status(201).json(errors)
    }
    const body = JSON.parse(JSON.stringify(req.body));
    const fileName = body.name + ".jpg" // Forces to be a jpg (works well)
    const filePath = `${__dirname}\\..\\client\\public\\uploads\\profile\\${fileName}`
    let filePathDatabase = ""
    try {
        fs.writeFileSync(filePath, req.file.buffer)
        filePathDatabase = "./uploads/profile/" + fileName
    } catch (err) {
        console.log(err)
    }
    try {
        await UserModel.findByIdAndUpdate(
            req.body.userId,
            {$set: {picture: filePathDatabase}},
            {new: true, upsert: true, setDefaultsOnInsert: true},
            (err, docs) => {
                if (!err) return res.send(docs)
                else return res.status(500).send({message: err})
            }
        ).clone()
    } catch (err) {
        return res.status(500).send({message: err})
    }
}