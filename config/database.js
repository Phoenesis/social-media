const mongoose = require("mongoose");

//https://stackoverflow.com/questions/72421572/mongoose-schema-and-typescript-cloneobject-error-max-call-stack-exceeded
//https://www.youtube.com/watch?v=SUPDFHuvhRc&ab_channel=FromScratch-D%C3%A9veloppementWeb ==> 37min11

mongoose
    .connect(
        "mongodb+srv://" + process.env.DB_USER_PASS + "@cluster0.ubsn2hj.mongodb.net/mern-project",
        {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            //useCreateIndex: true,
            //useFindAndModify: false
        })
    .then(() => console.log("Connected to MongoDB"))
    .catch((err) => console.log("Failed to connect to MongoDB : " + err))

