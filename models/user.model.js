const mongoose = require("mongoose");
const isEmail = require("validator");
const bcrypt = require("bcrypt");

/**
 * @BUG : validate: isEmail makes an error cloneObject, I don't know why, replaced by validateD
 * So maybe make a function checkMail in utils/validate.js
 * (link : https://www.tabnine.com/code/javascript/functions/validator/isEmail
 */
const userSchema = mongoose.Schema(
    {
        pseudo: {
            type: String,
            required: true,
            minlength: 3,
            maxlength: 55,
            unique: true,
            trim: true
        },
        email: {
            type: String,
            required: true,
            unique: true,
            //validate: isEmail,
            validated: false,
            lowercase: true,
            trim: true
        },
        password: {
            type: String,
            required: true,
            minlength: 6,
            maxlength: 1024
        },
        picture: {
            type: String,
            default: "./uploads/profile/random-user.jpg"
        },
        biography: {
            type: String,
            maxLength: 1024
        },
        followers: {
            type: [String]
        },
        following: {
            type: [String]
        },
        likers: {
            type: [String]
        }
    },
    {
        timestamps: true
    }
);

// play function before save into display: block
userSchema.pre("save", async function(next) {
    const salt = await bcrypt.genSalt()
    this.password = await bcrypt.hash(this.password, salt)
    next()
})

userSchema.statics.login = async function(email, password) {
    const user = await this.findOne({email})
    if (user) {
        const auth = await bcrypt.compare(password, user.password)
        if (auth) {
            return user
        }
        throw Error('incorrect password')
    }
    throw Error('incorrect email')
}

module.exports = mongoose.model("user", userSchema);