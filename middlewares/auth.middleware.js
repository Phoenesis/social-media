const jwt = require('jsonwebtoken')
const UserModel = require('../models/user.model')

module.exports.checkUser = (req, res, next) => {
    const token = req.cookies.jwt
    if (token) {
        jwt.verify(token, process.env.TOKEN_SECRET, {}, async (err, decodedToken) => {
            if (err) {
                res.locals.user = null
                res.cookie('jwt', '', {maxAge: 1})
                next()
            } else {
                res.locals.user = UserModel.findById(decodedToken.id)
                next()
            }
        })
    } else {
        res.locals.user = null
        next()
    }
}

module.exports.requireAuth = (req, res, next) => {
    const token = req.cookies.jwt
    if (token && token !== "undefined") {
        jwt.verify(token, process.env.TOKEN_SECRET, {}, async (err, decodedToken) => {
            if (err) {
                console.log(err)
            } else {
                res.status(200).send(decodedToken.id)
                next()
            }
        })
    } else {
        console.log("No token or jwt malformed")
    }
    //res.status(409).send("Unauthorized!")
    //next()
}