import React, {useEffect, useState} from 'react';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import Home from "./pages/Home";
import Trending from "./pages/Trending";
import Profile from "./pages/Profile";
import Settings from "./pages/Settings";
import NotFound from "./pages/NotFound";
import {ThemeContext, UidContext} from "./components/AppContext";
import axios from "axios";
import NavBar from "./components/NavBar";
import {useDispatch} from "react-redux";
import {getUser} from "./actions/user.actions";

/******************************************************************************************
 * https://www.youtube.com/watch?v=ghdRD3pt8rg&ab_channel=FromScratch-D%C3%A9veloppementWeb
 ******************************************************************************************/


const App = () => {
    const [uid, setUid] = useState(null);
    //const [theme, setTheme] = useState(true);
    const dispatch = useDispatch();
    const theme = true;


    useEffect(() => {
        const fetchToken = async () => {
            await axios({
                method: "get",
                url: `${process.env.REACT_APP_API_URL}jwtid`,
                withCredentials: true
            })
                .then((res) => {
                    setUid(res.data) // Contains the encrypted token, so the encrypted id
                })
                .catch((err) => console.log(err))
        }
        fetchToken()
            .then(() => {
                if (uid) {
                    dispatch(getUser(uid));
                }
            }
        );
    }, [uid, dispatch]);

    return (
        <div>
            <UidContext.Provider value={uid}>
                <ThemeContext.Provider value={theme}>
                    <BrowserRouter>
                        <NavBar/>
                        <Routes>
                            <Route path={"/"} element={<Home/>}/>
                            <Route path={"/trending"} element={<Trending/>}/>
                            <Route path={"/profile"} element={<Profile/>}/>
                            <Route path={"/settings"} element={<Settings/>}/>
                            <Route path={"*"} element={<NotFound/>}/>
                        </Routes>
                    </BrowserRouter>
                </ThemeContext.Provider>
            </UidContext.Provider>
        </div>
    );
};

export default App;