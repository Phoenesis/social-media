import {GET_USERS} from "../actions/others.actions";


const initialState = {}

export default function othersReducer(state = initialState, action) {
    switch (action.type) {
        case GET_USERS:
            return action.payload
        default:
            return state
    }
}