import {combineReducers} from "redux";
import userReducer from "./user.reducer";
import othersReducer from "./others.reducer";
import postReducer from "./post.reducer";
import postsReducer from "./posts.reducer";
import trendsReducer from "./trends.reducer";
import errorsReducer from "./errors.reducer";


export default combineReducers({userReducer, othersReducer, postReducer, postsReducer, trendsReducer, errorsReducer})
