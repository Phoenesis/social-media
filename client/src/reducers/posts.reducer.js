import {GET_ALL_POSTS} from "../actions/post.actions";

const initialPosts = {}

export default function postsReducer(state = initialPosts, action) {
    switch (action.type) {
        case GET_ALL_POSTS:
            return action.payload
        default:
            return state
    }
}