import React from 'react';
import "../styles/pages/_settingsPage.scss"
import LeftNav from "../components/LeftNav";

const Settings = () => {
    return (
        <div className={"settings-page"}>
            <LeftNav/>
            <h1> Settings </h1>
            <div className={"settings-container"}>
                <div className={"settings-left-part"}>
                    <h3> Settings part 1</h3>
                    <ul>
                        <li>Change pseudo</li>
                        <li>Change picture</li>
                        <li>Change password</li>
                        <li>Delete account</li>
                    </ul>
                </div>
                <div className={"settings-right-part"}>
                    <h3> Settings part 2</h3>
                    <ul>
                        <li className={"toggle-dark-mode"}>
                            Dark mode :
                            <label htmlFor={"dark-mode"} className={"label-dark-mode"}>
                                <i className={"fas fa-moon"}></i>
                                <i className={"fas fa-sun"}></i>
                                <div className={"ball-dark-mode"}></div>
                            </label>
                            <input type="checkbox" id={"dark-mode"} className={"checkbox-dark-mode"}/>
                        </li>
                        <li>Language</li>
                        <li>See blocked people</li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default Settings;
