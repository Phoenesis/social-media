import React, {useContext} from 'react';
import Log from "../components/Log";
import {UidContext} from "../components/AppContext";
import UpdateProfile from "../components/UpdateProfile";
import Footer from "../components/Footer";

const Profile = () => {
    const uid = useContext(UidContext);

    return (
        <div className={"profile-page"}>
            {uid ?
                (<UpdateProfile/>)
                : (<div className="log-container">
                    <Log signin={false} signup={true}/>
                    <div className="img-container">
                        <img src="./img/log.svg" alt="Log"/>
                    </div>
                </div>)
            }
            <Footer/>
        </div>
    );
};

export default Profile;
