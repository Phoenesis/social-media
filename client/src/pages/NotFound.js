import React from 'react';
import {NavLink} from "react-router-dom";
import "../styles/pages/_notFound.scss"

const NotFound = () => {
    return (
        <div className={"notFound-container"}>
            <h3> Oops ! 404 </h3>
            <p> Page not found </p>
            <NavLink to={"/"}>
                <i className={"fas fa-home"}></i>
                <span> Back to home </span>
            </NavLink>
        </div>
    );
};

export default NotFound;
