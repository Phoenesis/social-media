import axios from "axios";

export const GET_POSTS = "GET_POSTS"
export const GET_ALL_POSTS = "GET_ALL_POSTS"
export const LIKE_POST = "LIKE_POST"
export const UNLIKE_POST = "UNLIKE_POST"
export const UPDATE_POST = "UPDATE_POST"
export const DELETE_POST = "DELETE_POST"

export const ADD_COMMENT = "ADD_COMMENT"
export const EDIT_COMMENT = "EDIT_COMMENT"
export const DELETE_COMMENT = "DELETE_COMMENT"

export const GET_TRENDS = "GET_TRENDS"

export const GET_POSTS_ERRORS = "GET_POSTS_ERRORS"

export const HOW_MANY_POSTS_BY_LOAD = 5


export const getPosts = (howManyPosts = HOW_MANY_POSTS_BY_LOAD) => {
    return (dispatch) => {
        return axios
            .get(`${process.env.REACT_APP_API_URL}api/post`)
            .then(res => {
                const array = res.data.slice(0, howManyPosts)
                dispatch({type: GET_POSTS, payload: array})
                dispatch({type: GET_ALL_POSTS, payload: res.data})
            })
            .catch(err => console.log(err))
    }
}

export const createPost = (data) => {
    return () => {
        return axios({
            method: "post",
            url: `${process.env.REACT_APP_API_URL}api/post`,
            data: data
        })
            .catch(err => console.error(err))
    }
}

export const likePost = (postId, uid) => {
    return (dispatch) => {
        return axios({
            method: "patch",
            url: `${process.env.REACT_APP_API_URL}api/post/like-post/${postId}`,
            data: {id: uid}
        })
            .then(() => {dispatch({type: LIKE_POST, payload: {postId, uid}})})
            .catch(err => console.log(err))
    }
}

export const unlikePost = (postId, uid) => {
    return (dispatch) => {
        return axios({
            method: "patch",
            url: `${process.env.REACT_APP_API_URL}api/post/unlike-post/${postId}`,
            data: {id: uid}
        })
            .then(() => {dispatch({type: UNLIKE_POST, payload: {postId, uid}})})
            .catch(err => console.log(err))
    }
}

export const updatePost = (postId, message) => {
    return (dispatch) => {
        return axios({
            method: "put",
            url: `${process.env.REACT_APP_API_URL}api/post/${postId}`,
            data: {message}
        })
            .then(() => dispatch({type: UPDATE_POST, payload: {message, postId}}))
            .catch(err => console.log(err))
    }
}

export const deletePost = (postId) => {
    return (dispatch) => {
        return axios({
            method: "delete",
            url: `${process.env.REACT_APP_API_URL}api/post/${postId}`
        })
            .then(() => dispatch({type: DELETE_POST, payload: {postId}}))
            .catch(err => console.log(err))
    }
}

export const addComment = (postId, commenterId, text, commenterPseudo) => {
    return (dispatch) => {
        return axios({
            method: "patch",
            url: `${process.env.REACT_APP_API_URL}api/post/comment-post/${postId}`,
            data: {commenterId: commenterId, text: text, commenterPseudo: commenterPseudo}
        })
            .then(() => dispatch({type: ADD_COMMENT, payload: {postId}}))
            .catch(err => console.log(err))
    }
}

export const editComment = (postId, commentId, text) => {
    return (dispatch) => {
        return axios({
            method: "patch",
            url: `${process.env.REACT_APP_API_URL}api/post/edit-comment-post/${postId}`,
            data: {commentId: commentId, text: text}
        })
            .then(() => dispatch({type: EDIT_COMMENT, payload: {postId, commentId, text}}))
            .catch(err => console.log(err))
    }
}

export const deleteComment = (postId, commentId) => {
    return (dispatch) => {
        return axios({
            method: "patch",
            url: `${process.env.REACT_APP_API_URL}api/post/delete-comment-post/${postId}`,
            data: {commentId: commentId}
        })
            .then(() => dispatch({type: DELETE_COMMENT, payload: {postId, commentId}}))
            .catch(err => console.log(err))
    }
}

export const getTrends = (sortedArray) => {
    return (dispatch) => {
        dispatch({type: GET_TRENDS, payload: sortedArray})
    }
}
