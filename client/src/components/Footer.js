import React from 'react';

const Footer = () => {
    return (
        <div className={"footer"}>
            <p> Inspiration :
                <a href="https://youtu.be/SUPDFHuvhRc" target={"_blank"} rel="noreferrer"> Back-end </a>
                &
                <a href="https://youtu.be/ghdRD3pt8rg" target={"_blank"} rel="noreferrer"> Front-end </a>
                (videos on YouTube)
            </p>
            <p>
                Justine
            </p>
            <p> 2022 </p>
        </div>
    );
};

export default Footer;
