import React, {useContext, useEffect, useState} from 'react';
import {UidContext} from "./AppContext";
import {useDispatch} from "react-redux";
import {deleteComment, editComment} from "../actions/post.actions";

const EditDeleteComments = ({comment, postId}) => {
    const [isAuthor, setIsAuthor] = useState(false);
    const [isEditing, setIsEditing] = useState(false);
    const [text, setText] = useState("");
    const uid = useContext(UidContext);
    const dispatch = useDispatch();

    const handleEditComment = (event) => {
        event.preventDefault();

        if (text) {
            dispatch(editComment(postId, comment._id, text))
            setText("")
            setIsEditing(false)
        }
    }

    const handleDeleteComment = () => {
        dispatch(deleteComment(postId, comment._id))
    }

    useEffect(() => {
        const checkAuthor = () => {
            if (uid === comment.commenterId) setIsAuthor(true)
        }
        checkAuthor();
    }, [uid, comment.commenterId])

    return (
        <div className={"edit-comment"}>
            {isAuthor && isEditing === false && (
                <span onClick={() => setIsEditing(!isEditing)}>
                    <img src="./img/icons/edit.svg" alt="edit-pic"/>
                </span>
            )}
            {isAuthor && isEditing && (
                <form onSubmit={handleEditComment} className={"edit-comment-form"}>
                    <label htmlFor="text" onClick={() => setIsEditing(!isEditing)}> Edit </label>
                    <br/>
                    <input type="text"
                           name={"text"}
                           //value={text}
                           defaultValue={comment.text}
                           onChange={(e) => setText(e.target.value)}
                    />
                    <br/>
                    <div className={"btn"}>
                        <span onClick={() => {
                            if (window.confirm("Do you really want to delete this comment ?")) {
                                handleDeleteComment()
                            }
                        }}>
                            <img src="./img/icons/trash.svg" alt="trash"/>
                        </span>
                        <input type="submit" value={"Validate modifications"}/>
                    </div>
                </form>
            )}
        </div>
    );
};

export default EditDeleteComments;
