import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
//import Modal from "react-bootstrap/Modal";
//import Button from "react-bootstrap/Button";
import {dateParser, isEmpty} from "./Utils";
import FollowHandler from "./FollowHandler";
import LikeButton from "./LikeButton";
import {deletePost, updatePost} from "../actions/post.actions";
import CardComments from "./CardComments";


const Card = ({post}) => {
    const [isLoading, setIsLoading] = useState(true);
    const [isUpdated, setIsUpdated] = useState(false);
    const [textUpdate, setTextUpdate] = useState(null);
    const [showComment, setShowComment] = useState(false);
    //const [showConfirmModal, setShowConfirmModal] = useState(false);
    const othersData = useSelector(state => state.othersReducer);
    const userData = useSelector(state => state.userReducer);
    const dispatch = useDispatch();

    const updateItem = () => {
        if (textUpdate) {
            dispatch(updatePost(post._id, textUpdate));
        }
        setIsUpdated(false);
    }

    const deleteCard = () => {
        if (window.confirm("Do you really want to delete this post ?"))
            dispatch(deletePost(post._id));
    }


    /*
    <Modal show={showConfirmModal}
                                           onHide={() => setShowConfirmModal(false)}>
                                        <Modal.Header closeButton>
                                            <Modal.Title>Modal heading</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>You're reading this text in a modal!</Modal.Body>
                                        <Modal.Footer>
                                            <Button variant="secondary" onClick={() => setShowConfirmModal(false)}>
                                                Close
                                            </Button>
                                            <Button variant="primary" onClick={() => setShowConfirmModal(false)}>
                                                Save Changes
                                            </Button>
                                        </Modal.Footer>
                                    </Modal>
     */

    useEffect(() => {
        !isEmpty(othersData[0]) && setIsLoading(false);
        return () => {
        };
    }, [othersData]);


    return (
        <li className={"card-container"} key={post._id}>
            {
                isLoading ? (
                    <i className={"fas fa-spinner fa-spin"}></i>
                ) : (
                    <>
                        <div className={"card-left"}>
                            <img src={!isEmpty(othersData[0]) && othersData.map((user) => {
                                if (user._id === post.posterId) {
                                    return user.picture
                                } else {
                                    return null
                                }
                            }).join('')
                            } alt="poster-pic"/>
                        </div>
                        <div className="card-right">
                            <div className={"card-header"}>
                                <div className="pseudo">
                                    <h3> {
                                        !isEmpty(othersData[0]) && othersData.map((user) => {
                                        if (user._id === post.posterId) {
                                            return user.pseudo
                                        } else {
                                            return null
                                        }
                                        }).join('')
                                        }
                                    </h3>
                                    {post.posterId !== userData._id && (
                                        <FollowHandler idToFollow={post.posterId} type={"card"}/>
                                    )}
                                </div>
                                <span> {dateParser(post.createdAt)} </span>
                            </div>
                            {isUpdated === false && <p> {post.message} </p>}
                            {isUpdated === true && (
                                <div className={"update-post"}>
                                    <textarea defaultValue={post.message} onChange={(e) => setTextUpdate(e.target.value)}/>
                                    <div className="button-container">
                                        <button className="btn" onClick={updateItem}> Modify </button>
                                    </div>
                                </div>
                            )}
                            {post.picture && <img src={post.picture} className={"card-pic"} alt="post-pic"/>}
                            {post.video && (
                                <iframe width={"500"}
                                        height={"300"}
                                        src={post.video}
                                        frameBorder={"0"}
                                        title="YouTube video player"
                                        allowFullScreen
                                        allow={"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"}>
                                </iframe>
                            )}
                            {userData._id === post.posterId && (
                                <div className={"button-container"}>
                                    <div onClick={() => setIsUpdated(!isUpdated)}>
                                        <img src="./img/icons/edit.svg" alt="edit"/>
                                    </div>
                                    <div onClick={() => deleteCard()}>
                                        <img src="./img/icons/trash.svg" alt="trash"/>
                                    </div>
                                </div>
                            )}
                            <div className={"card-footer"}>
                                <div className={"comment-icon"}>
                                    <img onClick={() => setShowComment(!showComment)} src="./img/icons/message1.svg" alt="comment"/>
                                    <span>{post.comments.length}</span>
                                </div>
                                {userData._id !== post.posterId && <LikeButton post={post}/>}
                                <img src="./img/icons/share.svg" alt="share"/>
                            </div>
                            {showComment && <CardComments post={post}/>}
                        </div>
                    </>
                )}
        </li>
    );
};

export default Card;
