import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {uploadPicture} from "../actions/user.actions";


const UploadPicture = () => {
    const [file, setFile] = useState();
    const dispatch = useDispatch();
    const userData = useSelector(state => state.userReducer)

    const handlePicture = (event) => {
        event.preventDefault()
        const data = new FormData();
        data.append("name", userData.pseudo)
        data.append("userId", userData._id)
        data.append("file", file)
        dispatch(uploadPicture(data, userData._id))
    }

    return (
        <form action="" onSubmit={handlePicture} className={"update-pic"}>
            {file ? <label htmlFor="file"> {file.name} </label>
                : <label htmlFor="file"> Change the picture </label>
            }
            <input type="file" id={"file"} name={"file"} accept=".jpg, .jpeg, .png"
                   onChange={(e) => {setFile(e.target.files[0])
                       console.log(e.target.files[0])}}/>
            <br/>
            {file && <input type="submit" value={"Send"}/>}
        </form>
    );
};

export default UploadPicture;
