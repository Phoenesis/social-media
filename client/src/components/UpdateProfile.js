import React, {useState} from 'react';
import LeftNav from "./LeftNav";
import {useDispatch, useSelector} from "react-redux";
import UploadPicture from "./UploadPicture";
import {updateBio} from "../actions/user.actions";
import {dateParser} from "./Utils";
import FollowHandler from "./FollowHandler";

const UpdateProfile = () => {
    const [bio, setBio] = useState("");
    const [updateForm, setUpdateForm] = useState(false);
    const [followingPopup, setFollowingPopup] = useState(false);
    const [followersPopup, setFollowersPopup] = useState(false);
    const userData = useSelector(state => state.userReducer);
    const othersData = useSelector(state => state.othersReducer);
    const errors = useSelector(state => state.errorsReducer.userErrors)
    const dispatch = useDispatch();

    const handleUpdate = () => {
        dispatch(updateBio(bio, userData._id))
        setUpdateForm(false)
    }


    return (
        <div className={"profile-page"}>
            <LeftNav/>
            <h1> Profile of "{userData.pseudo}"</h1>
            <div className={"update-container"}>
                <div className={"left-part"}>
                    <h3> Photo </h3>
                    <img src={userData.picture ? userData.picture : "./uploads/profile/random-user.jpg"} alt="user-pic"/>
                    <UploadPicture/>
                    <p> {errors.maxSize} </p>
                    <p> {errors.format} </p>
                </div>
                <div className={"right-part"}>
                    <div className={"bio-update"}>
                        <h3> Biography </h3>
                        {updateForm === false && (
                            <>
                                <p onClick={() => setUpdateForm(!updateForm)}> {userData.biography} </p>
                                <button onClick={() => setUpdateForm(!updateForm)}> Update bio</button>
                            </>
                        )}
                        {updateForm === true && (
                            <>
                                <textarea defaultValue={userData.biography}
                                          onChange={(e) => {
                                              setBio(e.target.value)
                                          }}></textarea>
                                <button onClick={handleUpdate}> Modify</button>
                            </>
                        )}
                    </div>
                    <h4> Member since {dateParser(userData.createdAt)}</h4>
                    <h5 onClick={() => setFollowingPopup(true)}> Followings
                        : {userData.following ? userData.following.length : "0"} </h5>
                    <h5 onClick={() => setFollowersPopup(true)}> Followers
                        : {userData.followers ? userData.followers.length : "0"} </h5>
                </div>
            </div>
            {
                followingPopup && <div className={"popup-profile-container"}>
                    <div className="modal">
                        <h3> Followings </h3>
                        <span className={"cross"} onClick={() => setFollowingPopup(false)}>&#10005;</span>
                        <ul>
                            {othersData.map((user) => {
                                for (let i = 0; i < userData.following.length; i++) {
                                    if (user._id === userData.following[i]) {
                                        return (
                                            <li key={user._id}>
                                                <img src={user.picture} alt="user-pic"/>
                                                <h4>{user.pseudo}</h4>
                                                <div className="follow-handler">
                                                    <FollowHandler idToFollow={user._id} type={"suggestion"}/>
                                                </div>
                                            </li>
                                        )
                                    }
                                }
                                return null
                            })}
                        </ul>
                    </div>
                </div>
            }
            {
                followersPopup && <div className={"popup-profile-container"}>
                    <div className="modal">
                        <h3> Followers </h3>
                        <span className={"cross"} onClick={() => setFollowersPopup(false)}>&#10005;</span>
                        <ul>
                            {othersData && othersData.map((user) => {
                                for (let i = 0; i < userData.followers.length; i++) {
                                    if (user._id === userData.followers[i]) {
                                        return (
                                            <li key={user._id}>
                                                <img src={user.picture} alt="user-pic"/>
                                                <h4>{user.pseudo}</h4>
                                                <div className="follow-handler">
                                                    <FollowHandler idToFollow={user._id} type={"suggestion"}/>
                                                </div>
                                            </li>
                                        )
                                    }
                                }
                                return null
                            })}
                        </ul>
                    </div>
                </div>
            }
        </div>
    );
};

export default UpdateProfile;
