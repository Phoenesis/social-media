import React, {useContext, useEffect, useState} from 'react';
import {UidContext} from "./AppContext";
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import {useDispatch} from "react-redux";
import {likePost, unlikePost} from "../actions/post.actions";

const LikeButton = ({post}) => {
    const [isLiked, setIsLiked] = useState(false);
    const uid = useContext(UidContext);
    const dispatch = useDispatch();

    const like = () => {
        dispatch(likePost(post._id, uid))
        setIsLiked(true)
    }

    const unlike = () => {
        dispatch(unlikePost(post._id, uid))
        setIsLiked(false)
    }

    useEffect(() => {
        if (post.likers.includes(uid)) {
            setIsLiked(true)
        } else {
            setIsLiked(false)
        }
        return () => {
        };
    }, [uid, post.likers, isLiked]);


    return (
        <div className={"like-container"}>
            {uid === null &&
            <Popup trigger={<img src="./img/icons/heart.svg" alt="like"/>}
                   position={['bottom center', 'bottom right', 'bottom left']}
                   closeOnDocumentClick>
                <div> Please login to like a post ! </div>
            </Popup>}
            {uid && isLiked === false && (
                <img src="./img/icons/heart.svg" onClick={like} alt="like"/>
            )}
            {uid && isLiked === true && (
                <img src="./img/icons/heart-filled.svg" onClick={unlike} alt="like-filled"/>
            )}
            <span> {post.likers.length} </span>
        </div>
    );
};

export default LikeButton;
