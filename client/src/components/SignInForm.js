import React, {useState} from 'react';
import axios from "axios";

const SignInForm = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const handleLogin = (e) => {
        e.preventDefault();
        const emailError = document.querySelector('.email.error');
        const passwordError = document.querySelector('.password.error');
        axios({
            method: "post",
            url: `${process.env.REACT_APP_API_URL}api/user/login`,
            withCredentials: true,
            data: {
                email: email,
                password: password
            }
        }).then((res) => {
            console.log(res)
            if (res.data.errors) {
                console.log(res.data)
                emailError.innerHTML = res.data.errors.email;
                passwordError.innerHTML = res.data.errors.password;
            } else {
                window.location = '/';
            }
        }).catch(err => console.log(err))
    }

    return (
        <form id={"sign-in-form"} onSubmit={handleLogin}>
            <label htmlFor="email"> Email </label>
            <br/>
            <input id={"email"}
                   type="text"
                   name={"email"}
                   value={email}
                   onChange={(e) => setEmail(e.target.value)}/>
            <div className={"email error"}></div>
            <br/>
            <label htmlFor="password"> Password </label>
            <br/>
            <input id={"password"}
                   type="password"
                   name={"password"}
                   value={password}
                   onChange={e => setPassword(e.target.value)}/>
            <div className="password error"></div>
            <br/>
            <input type="submit" value={"Sign In"}/>
        </form>
    );
};

export default SignInForm;
