import React, {useEffect, useState} from 'react';
import {useSelector} from "react-redux";
import {isEmpty} from "./Utils";
import FollowHandler from "./FollowHandler";

const FriendsHint = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [playOnce, setPlayOnce] = useState(true);
    const [friendsHint, setFriendsHint] = useState([]);
    const userData = useSelector(state => state.userReducer);
    const othersData = useSelector(state => state.othersReducer);

    useEffect(() => {
        if (playOnce && !isEmpty(othersData[0]) && !isEmpty(userData._id)) {
            const notInFriendList = () => {
                let array = [];
                othersData.map((user) => {
                    if (userData._id !== user._id && !user.followers.includes(userData._id)) {
                        return array.push(user._id)
                    } else {
                        return null
                    }
                })
                array.sort(() => 0.5 - Math.random())
                if (window.innerHeight > 780) {
                    array.length = 5
                } else if (window.innerHeight > 720 && window.innerHeight <= 780) {
                    array.length = 4
                } else if (window.innerHeight > 615 && window.innerHeight <= 720) {
                    array.length = 3
                } else if (window.innerHeight > 540 && window.innerHeight <= 615) {
                    array.length = 1
                } else {
                    array.length = 0
                }
                setFriendsHint(array)
            }
            notInFriendList();
            setIsLoading(false);
            setPlayOnce(false);
        }
    }, [userData, othersData, playOnce]);


    return (
        <div className={"get-friends-container"}>
            <h4> Suggestions </h4>
            {isLoading ? (
                <div className={"icon"}>
                    <i className={"fas fa-spinner fa-pulse"}></i>
                </div>
            ) : (
                <ul>
                    {friendsHint && friendsHint.map((user) => {
                        for (let i = 0; i < othersData.length; i++) {
                            if (user === othersData[i]._id) {
                                return (
                                    <li key={user} className={"user-hint"}>
                                        <img src={othersData[i].picture} alt="pic"/>
                                        <p> {othersData[i].pseudo} </p>
                                        <FollowHandler idToFollow={othersData[i]._id} type={"suggestion"}/>
                                    </li>
                                )
                            }
                        }
                        return null
                    })}
                </ul>
            )}
        </div>
    );
};

export default FriendsHint;
