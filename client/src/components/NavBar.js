import React, {useContext} from 'react';
import {NavLink} from "react-router-dom";
import {UidContext} from "./AppContext";
import Logout from "./Logout";
import {useSelector} from "react-redux";

const NavBar = () => {
    const uid = useContext(UidContext)
    const userData = useSelector((state) => state.userReducer)
    return (
        <nav>
            <div className="nav-container">
                <NavLink to={"/"}>
                    <div className="logo">
                        <img src="./img/badgerNavBar.png" alt="icon"/>
                        <h3> Badger Network </h3>
                    </div>
                </NavLink>
                {uid ? (
                    <ul>
                        <li></li>
                        <li className={"welcome"}>
                            <NavLink to={"/profile"}>
                                <h5> Welcome {userData.pseudo} </h5>
                            </NavLink>
                        </li>
                        <Logout/>
                    </ul>
                ) : (
                    <ul>
                        <li></li>
                        <li>
                            <NavLink to={"/profile"}>
                                <img src="./img/icons/login.svg" alt="login"/>
                            </NavLink>
                        </li>
                    </ul>
                )}
            </div>
        </nav>
    );
};

export default NavBar;
