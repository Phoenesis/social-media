import React, {useState} from 'react';
import axios from "axios";
import SignInForm from "./SignInForm";

const SignUpForm = () => {
    const [formSubmit, setFormSubmit] = useState(false);
    const [pseudo, setPseudo] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [controlledPassword, setControlledPassword] = useState("");

    const handleRegister = async (event) => {
        event.preventDefault()
        const terms = document.getElementById("terms")
        const pseudoError = document.querySelector(".pseudo.error")
        const emailError = document.querySelector(".email.error")
        const passwordError = document.querySelector(".password.error")
        const controlledPasswordError = document.querySelector(".password-confirm.error")
        const termsError = document.querySelector(".terms.error")

        controlledPasswordError.innerHTML = ""
        termsError.innerHTML = ""

        if (password !== controlledPassword || !terms.checked) {
            if (password !== controlledPassword) {
                controlledPasswordError.innerHTML = "Please give similar passwords"
            }
            if (!terms.checked) {
                termsError.innerHTML = "Please accept the terms"
            }
        } else {
            await axios({
                method: "post",
                url: `${process.env.REACT_APP_API_URL}api/user/register`,
                data: {
                    pseudo: pseudo,
                    email: email,
                    password: password
                }
            }).then((res) => {
                if (res.data.errors) {
                    pseudoError.innerHTML = res.data.errors.pseudo
                    emailError.innerHTML = res.data.errors.email
                    passwordError.innerHTML = res.data.errors.password
                } else {
                    setFormSubmit(true)
                }
            }).catch((err) => console.log(err))
        }
    }

    return (
        <>
            {formSubmit
                ? (<>
                    <SignInForm/>
                    <span></span>
                    <h4 className={"success"}> Registry successful, now please login</h4>
                </>)
                : (
                    <form id={"sign-up-form"} onSubmit={handleRegister}>
                        <label htmlFor="pseudo"> Pseudo </label>
                        <br/>
                        <input id={"pseudo"}
                               type="text"
                               name={"pseudo"}
                               value={pseudo}
                               onChange={e => setPseudo(e.target.value)}/>
                        <div className="pseudo error"></div>
                        <br/>
                        <label htmlFor="email"> Email </label>
                        <br/>
                        <input id={"email"}
                               type="email"
                               name={"email"}
                               value={email}
                               onChange={e => setEmail(e.target.value)}/>
                        <div className="email error"></div>
                        <br/>
                        <label htmlFor="password"> Password </label>
                        <br/>
                        <input id={"password"}
                               type="password"
                               name={"password"}
                               value={password}
                               onChange={e => setPassword(e.target.value)}/>
                        <div className="password error"></div>
                        <br/>
                        <label htmlFor="password-conf"> Confirm password </label>
                        <br/>
                        <input id={"password-conf"}
                               type="password"
                               name={"password"}
                               value={controlledPassword}
                               onChange={e => setControlledPassword(e.target.value)}/>
                        <div className="password-confirm error"></div>
                        <br/>
                        <input id={"terms"} type="checkbox"/>
                        <label htmlFor={"terms"}> I accept the
                            <a href="/" target={"_blank"} rel={"noopener noreferrer"}>{' '}terms and conditions</a>
                        </label>
                        <div className="terms error"></div>
                        <br/>
                        <input type="submit" value={"Sign Up"}/>
                    </form>
                )
            }
        </>
    );
};

export default SignUpForm;
