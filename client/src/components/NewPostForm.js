import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {isEmpty, timestampParser} from "./Utils";
import {NavLink} from "react-router-dom";
import {createPost, getPosts} from "../actions/post.actions";

const NewPostForm = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [message, setMessage] = useState("");
    const [postPicture, setPostPicture] = useState(null);
    const [postVideo, setPostVideo] = useState("");
    const [postFile, setPostFile] = useState("");
    const userData = useSelector(state => state.userReducer);
    const errors = useSelector(state => state.errorsReducer.postErrors);
    const dispatch = useDispatch();

    const handlePost = async () => {
        if (message || postPicture || postVideo) {
            const data = new FormData()
            data.append("posterId", userData._id)
            data.append("message", message)
            if (postFile) data.append("picture", postFile)
            data.append("video", postVideo)
            await dispatch(createPost(data))
            dispatch(getPosts())
            cancelPost()

        } else {
            alert(" Please write something ");
        }
    }

    //TODO
    const handlePicture = (event) => {
        setPostPicture(URL.createObjectURL(event.target.files[0]));
        //console.log("URL.createObjectURL(event.target.files[0])")
        //console.log(URL.createObjectURL(event.target.files[0]))
        //console.log(typeof URL.createObjectURL(event.target.files[0]))
        //console.log(postPicture)
        //console.log("End...")
        setPostFile(event.target.files[0]);
        setPostVideo("");
    }

    const cancelPost = () => {
        setMessage("")
        setPostPicture("")
        setPostVideo("")
        setPostFile("")
    }

    useEffect(() => {
        if (!isEmpty(userData)) {
            setIsLoading(false)
        }
        const handleVideo = () => {
            let findLink = message.split(" ");
            for (let i = 0; i<findLink.length; i++) {
                if (findLink[i].includes("https://www.youtube.com")) {
                    let embed = findLink[i].replace("watch?v=", "embed/")
                    setPostVideo(embed.split("&")[0])
                    findLink.splice(i, 1)
                    setMessage(findLink.join(" "))
                    setPostPicture("")
                }
                else if (findLink[i].includes("https://youtu.be")) {
                    let embed = findLink[i].replace("youtu.be", "www.youtube.com/embed")
                    setPostVideo(embed.split("&")[0])
                    findLink.splice(i, 1)
                    setMessage(findLink.join(" "))
                    setPostPicture("")
                }
            }
        }
        handleVideo();
    }, [userData, message, postVideo])


    return (
        <div className={"post-container"}>
            {isLoading ? (
                <i className={"fas fa-spinner fa-pulse"}></i>
            ) : (
                <>
                    <div className="data">
                        <p>
                            <span>
                                {userData.following ? userData.following.length : 0}
                            </span> Following{userData.following.length && userData.following.length > 1 ? "s" : ""}
                        </p>
                        <p>
                            <span>
                                {userData.followers ? userData.followers.length : 0}
                            </span> Following{userData.followers.length && userData.followers.length > 1 ? "s" : ""}
                        </p>
                    </div>
                    <NavLink to={"/profile"}>
                        <div className={"user-info"}>
                            <img src={userData.picture} alt="user-pic"/>
                        </div>
                    </NavLink>
                    <div className={"post-form"}>
                        <textarea id={"message"}
                                  name={"message"}
                                  value={message}
                                  placeholder={"Express yourself ! "}
                                  onChange={(e) => setMessage(e.target.value)}/>
                        {message || postPicture || postVideo.length > 5 ? (
                            <li className={"card-container"}>
                                <div className={"card-left"}>
                                    <img src={userData.picture} alt="user-pic"/>
                                </div>
                                <div className="card-right">
                                    <div className={"card-header"}>
                                        <div className={"pseudo"}>
                                            <h3> {userData.pseudo} </h3>
                                        </div>
                                        <span> {timestampParser(Date.now())} </span>
                                    </div>
                                    <div className={"content"}>
                                        <p> {message} </p>
                                        {postPicture && <img src={postPicture} alt="wanted-pic"/>}
                                        {postVideo && (
                                            <iframe width="560" height="315"
                                                    src={postVideo}
                                                    title={postVideo}
                                                    frameBorder="0"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media;
                                                    gyroscope; picture-in-picture"
                                                    allowFullScreen></iframe>
                                        )}
                                    </div>
                                </div>
                            </li>
                        ) : null}
                        <div className={"footer-form"}>
                            <div className={"icon"}>
                                {isEmpty(postVideo) && (
                                    <>
                                        <img src="./img/icons/picture.svg" alt="pic"/>
                                        <input id="file-upload" name={"picture"} type={"file"} accept={".jpg, .jpeg, .png"}
                                               onChange={(e) => handlePicture(e)}/>
                                    </>
                                )}
                                {postVideo && (
                                    <button onClick={() => setPostVideo("")}> Delete video </button>
                                )}
                            </div>
                            {errors && !isEmpty(errors.format && <p> {errors.format} </p>)}
                            {errors && !isEmpty(errors.maxSize && <p> {errors.maxSize} </p>)}
                            <div className={"btn-send"}>
                                {message || postPicture || postVideo.length > 5 ? (
                                    <button className={"cancel"} onClick={cancelPost}> Cancel </button>
                                ) : null}
                                <button className={"send"} onClick={handlePost}> Send </button>
                            </div>
                        </div>
                    </div>
                </>
            )}
        </div>
    );
};

export default NewPostForm;
