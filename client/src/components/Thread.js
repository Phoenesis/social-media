import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getPosts} from "../actions/post.actions";
import {isEmpty} from "./Utils";
import Card from "./Card";

const Thread = () => {
    const numberOfPostsByLoad = 5;
    const [loadPost, setLoadPost] = useState(true);
    const [counter, setCounter] = useState(numberOfPostsByLoad);
    const dispatch = useDispatch();
    const posts = useSelector(state => state.postReducer);

    const loadMore = () => {
        if (window.innerHeight + document.documentElement.scrollTop + 3 > document.scrollingElement.scrollHeight) {
            setLoadPost(true)
        }
    }

    useEffect(() => {
        if (loadPost) {
            dispatch(getPosts(counter));
            setLoadPost(false);
            setCounter(counter + numberOfPostsByLoad)
        }
        window.addEventListener('scroll', loadMore);
        return () => {
            window.removeEventListener('scroll', loadMore)
        };
    }, [loadPost, counter, dispatch]);


    return (
        <div className={"thread-container"}>
            <ul>
                {
                    !isEmpty(posts[0]) && posts.map((post) => {
                        return <Card key={post._id} post={post}/>
                    })
                }
            </ul>
        </div>
    );
};

export default Thread;
