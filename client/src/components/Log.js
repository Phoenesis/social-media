import React, {useState} from 'react';
import SignUpForm from "./SignUpForm";
import SignInForm from "./SignInForm";

const Log = ({signin, signup}) => {
    const [signUpModal, setSignUpModal] = useState(signup);
    const [signInModal, setSignInModal] = useState(signin);

    const handleModals = (e) => {
        if (e.target.id === "register") {
            setSignInModal(false)
            setSignUpModal(true)
        } else if (e.target.id === "login") {
            setSignUpModal(false)
            setSignInModal(true)
        }
    }

    return (
        <div className={"connection-form"}>
            <div className="form-container">
                <ul>
                    <li className={signUpModal ? "active-btn" : null} id={"register"} onClick={handleModals}>Register</li>
                    <li id={"login"} className={signInModal ? "active-btn" : null} onClick={handleModals}>Login</li>
                </ul>
                {signUpModal && <SignUpForm/>}
                {signInModal && <SignInForm/>}
            </div>
        </div>
    );
};

export default Log;
