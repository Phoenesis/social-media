import {createContext} from "react";

export const UidContext = createContext(undefined);
export const ThemeContext = createContext(undefined);
