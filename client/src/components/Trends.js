import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {isEmpty} from "./Utils";
import {getTrends} from "../actions/post.actions";
import {NavLink} from "react-router-dom";

const Trends = () => {
    const posts = useSelector(state => state.postsReducer);
    const othersData = useSelector(state => state.othersReducer);
    const trendsList = useSelector(state => state.trendsReducer);
    const dispatch = useDispatch();

    useEffect(() => {
        if (!isEmpty(posts[0])) {
            const postsArray = Object.keys(posts).map(i => posts[i])
            let sortedArray = postsArray.sort((a, b) => {
                return b.likers.length - a.likers.length
            })
            sortedArray.length = 3;
            dispatch(getTrends(sortedArray))
        }
    }, [posts, dispatch]);


    return (
        <div className={"trending-container"}>
            <h4> Trending </h4>
            <NavLink to={"/trending"}>
                <ul>
                    {trendsList.length && trendsList.map((post) => {
                        return (
                            <li key={post._id}>
                                <div>
                                    {post.picture && <img src={post.picture} alt="pic"/>}
                                    {post.video && (
                                        <iframe width={"500"}
                                                height={"300"}
                                                src={post.video}
                                                frameBorder={"0"}
                                                title={post._id}
                                                allowFullScreen
                                                allow={"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"}>
                                        </iframe>
                                    )}
                                    {!post.picture && !post.video && (
                                        <img src={!isEmpty(othersData[0]) && othersData.map((user) => {
                                            if (user._id === post.posterId) {
                                                return user.picture
                                            } else {
                                                return null
                                            }
                                        }).join('')
                                        } alt="poster-pic"/>
                                    )}
                                </div>
                                <div className={"trend-content"}>
                                    <p> {post.message} </p>
                                    <span> Read more </span>
                                </div>
                            </li>
                        )
                    })}
                </ul>
            </NavLink>
        </div>
    );
};

export default Trends;
