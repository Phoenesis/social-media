import React from 'react';
import {NavLink} from "react-router-dom";
import {HashLink} from "react-router-hash-link";

// className={(active) => (active ? "active-left-nav" : "")}
const LeftNav = () => {
    return (
        <div className={"left-nav-container"}>
            <div className="icons">
                <div className={"icons-bis"}>
                    <NavLink to={"/"} className={({isActive}) => isActive ? "active-left-nav" : ""}>
                        <img src="./img/icons/home.svg" alt="home"/>
                    </NavLink>
                    <br/>
                    <NavLink to={"/trending"} className={({isActive}) => isActive ? "active-left-nav" : ""}>
                        <img src="./img/icons/rocket.svg" alt="posts"/>
                    </NavLink>
                    <br/>
                    <NavLink to={"/profile"} className={({isActive}) => (isActive ? "active-left-nav" : "")}>
                        <img src="./img/icons/user.svg" alt="profile"/>
                    </NavLink>
                    <HashLink to={"/#main-home-anchor"}>
                        <img src="./img/icons/home.svg" alt="up-arrow"/>
                    </HashLink>
                </div>
            </div>
        </div>
    );
};

export default LeftNav;
